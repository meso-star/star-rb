/* Copyright (C) |Meso|Star> 2015-2016 (contact@meso-star.com)
 *
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use,
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and, more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms. */

#include <rsys/clock_time.h>
#include <rsys/cstr.h>
#include <rsys/float3.h>
#include <rsys/mem_allocator.h>

#include <star/s3d.h>
#include <star/s3daw.h>
#include <star/smc.h>

#include "srb_realization.h"

#include <string.h>

#define MAX_KA 0.75
static double
ka_field_fn(const float pt[3]) {
  double ka = 0.25 + 0.5 / (1 + pt[0] * pt[0]);
  ASSERT(0 <= ka && ka <= MAX_KA);
  return ka;
}

#define MAX_KS 1.25
static double
ks_field_fn(const float pt[3]) {
  double ks = 0.75 + 0.5
    * sin(16 * PI * pt[0]) * sin(16 * PI * pt[1]) * sin(16 * PI * pt[2]);
  ASSERT(0 <= ks && ks <= MAX_KS);
  return ks;
}

static double
g_field_fn(const float pt[3]) {
  (void)pt;
  return 0.5;
}

static double
t_fluid_field_fn(const float pt[3]) {
  return 1000 + 500 * f3_len(pt);
}

static double
t_bound_field_fn(const float pt[3]) {
  (void)pt;
  return 400;
}

static double
eps_field_fn(const float pt[3]) {
  (void)pt;
  return 0.8;
}

static const double k = MAX_KA + MAX_KS;

static res_T
compute_rad_budget
  (struct s3d_scene* scene,
   const size_t max_steps,
   const float x0[3],
   double lambda,
   int verbose)
{
  struct time begin, end, elapsed;
  struct context ctx;
  struct smc_device* smc = NULL;
  struct smc_integrator integrator;
  struct smc_estimator* estimator = NULL;
  struct smc_estimator_status estimator_status;
  double budget, sig_budget;
  int mask;
  res_T res = RES_OK;
  ASSERT(scene && max_steps > 0);

  S3D(scene_begin_session(scene, S3D_SAMPLE|S3D_TRACE));

  /* Initialize context for MC computation */
  ctx.scene = scene;
  ctx.ka_field_fn = &ka_field_fn;
  ctx.ks_field_fn = &ks_field_fn;
  ctx.g_field_fn = &g_field_fn;
  ctx.t_fluid_field_fn = &t_fluid_field_fn;
  ctx.t_bound_field_fn = &t_bound_field_fn;
  ctx.eps_field_fn = &eps_field_fn;
  ctx.k = k;
  ctx.lambda = lambda;
  f3_set(ctx.x0, x0);

  /* Setup Star-MC */
  SMC(device_create(NULL, NULL, 1, NULL, &smc));
  integrator.integrand = &srb_realization; /* Realization function */
  integrator.type = &smc_double; /* Type of the Monte Carlo weight */
  integrator.max_steps = max_steps; /* Realization count */
  integrator.max_failures = 10; /* cancel integration if 10 realization fail */

  /* Solve (can return RES_BAD_OP if max_failures is reached) */
  time_current(&begin);
  res = smc_solve(smc, &integrator, &ctx, &estimator);
  ASSERT(res == RES_OK || res == RES_BAD_OP);
  time_current(&end);

  /* Print the simulation results */
  if(smc_estimator_get_status(estimator, &estimator_status) != RES_OK) {
    if (verbose) {
      fprintf(stderr,
	      "Cannot read the simulation status.\n"
              "The scene might not match the prerequisites:\n"
	      "it must be closed and its normals must point *into* the volume.\n");
    }
    goto error;
  }

  if(estimator_status.NF >= integrator.max_failures) {
    if (verbose) {
      fprintf(stderr,
	      "Too many failures (%lu). The scene might not match the prerequisites:\n"
	      "it must be closed and its normals must point *into* the volume.\n",
	      (unsigned long)estimator_status.NF);
    }
    res = RES_BAD_ARG;
    goto error;
  }
  budget = SMC_DOUBLE(estimator_status.E);
  sig_budget = SMC_DOUBLE(estimator_status.SE);
  if (verbose) {
    char buf[512];
    time_sub(&elapsed, &end, &begin);
    time_dump(&elapsed, TIME_MIN|TIME_SEC|TIME_MSEC, NULL, buf, sizeof(buf));
    printf("Radiative budget ~ %g +/- %g\n# failures: %lu/%lu\nElapsed time: %s\n",
	   budget, sig_budget,
	   (unsigned long)estimator_status.NF, (unsigned long)max_steps, buf);
  }
  else {
    printf("%g %g", budget, sig_budget);
  }

exit:
  /* Clean-up data */
  if(smc) SMC(device_ref_put(smc));
  if(estimator) SMC(estimator_ref_put(estimator));
  S3D(scene_get_session_mask(scene, &mask));
  if(mask) S3D(scene_end_session(scene));
  return res;

error:
  goto exit;
}

/* Create a S3D scene from an obj in a scene */
static res_T
import_obj(const char* filename, struct s3d_scene** out_scene)
{
  struct s3d_device* s3d = NULL;
  struct s3d_scene* scene = NULL;
  struct s3daw* s3daw = NULL;
  const int VERBOSE = 0;
  res_T res = RES_OK;
  ASSERT(out_scene);

  #define CALL(Func) if(RES_OK != (res = Func)) goto error
  CALL(s3d_device_create(NULL, NULL, VERBOSE, &s3d));
  CALL(s3daw_create(NULL, NULL, NULL, NULL, s3d, VERBOSE, &s3daw));
  CALL(s3daw_load(s3daw, filename));
  CALL(s3d_scene_create(s3d, &scene));
  CALL(s3daw_attach_to_scene(s3daw, scene));
  #undef CALL

exit:
  /* Release memory */
  if(s3daw) S3DAW(ref_put(s3daw));
  if(s3d) S3D(device_ref_put(s3d));
  *out_scene = scene;
  return res;
error:
  if(scene) {
    S3D(scene_ref_put(scene));
    scene = NULL;
  }
  goto exit;
}

int
main(int argc, char* argv[])
{
  struct s3d_scene* scene = NULL;
  unsigned long nsteps = 10000;
  float x0[3];
  double lambda;
  res_T res = RES_OK;
  int i, count_idx = 0, verbose = 1;
  int ret = EXIT_SUCCESS;

  /* Check command arguments */
  if(argc < 6 || argc > 8) {
    printf("Usage: %s OBJ_FILE LAMBDA X Y Z [SAMPLES_COUNT] [--quiet]\n", argv[0]);
    goto error;
  }

  /* Import file's content in the scene */
  res = import_obj(argv[1], &scene);
  if(res != RES_OK) {
    fprintf(stderr, "Couldn't import `%s'\n", argv[1]);
    goto error;
  }

  /* Set lambda */
  res = cstr_to_double(argv[2], &lambda);
  if(res != RES_OK || lambda <= 0) {
    fprintf(stderr, "Invalid lambda `%s'\n", argv[2]);
    goto error;
  }

  /* Set starting point */
  for(i = 0; i < 3; i++) {
    res = cstr_to_float(argv[3 + i], &x0[i]);
    if(res != RES_OK) {
      fprintf(stderr, "Invalid coordinate value `%s'\n", argv[3 + i]);
      goto error;
    }
  }

  /* detect optional arguments */
  if (argc == 8) {
    /* must be SAMPLES_COUNT --quiet */
    count_idx = 6;
    if (strcmp(argv[7], "--quiet") == 0)
      verbose = 0;
    else {
      fprintf(stderr, "Invalid argument `%s'\n", argv[7]);
      goto error;
    }
  }

  if (argc == 7) {
    if (strcmp(argv[6], "--quiet") == 0)
      verbose = 0;
    else
      /* must be SAMPLES_COUNT */
      count_idx = 6;
  }

  /* Set number of realizations */
  if(count_idx > 0) {
    res = cstr_to_ulong(argv[count_idx], &nsteps);
    if(res != RES_OK) {
      fprintf(stderr, "Invalid number of steps `%s'\n", argv[count_idx]);
      goto error;
    }
  }

  res = compute_rad_budget(scene, nsteps, x0, lambda, verbose);
  if(res != RES_OK) {
    fprintf(stderr, "Error in radiative budget integration\n");
    goto error;
  }

exit:
  if(scene) S3D(scene_ref_put(scene));
  if(mem_allocated_size() != 0) {
    fprintf(stderr, "Memory leaks: %lu Bytes\n",
      (unsigned long)mem_allocated_size());
    ret = EXIT_FAILURE;
  }
  return ret;
error:
  ret = EXIT_FAILURE;
  goto exit;
}

