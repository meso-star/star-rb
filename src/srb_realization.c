/* Copyright (C) |Meso|Star> 2015-2016 (contact@meso-star.com)
 *
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use,
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and, more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms. */

#include <rsys/rsys.h>
#include <rsys/float3.h>

#include <star/s3d.h>
#include <star/ssp.h>
#include <star/smc.h>

#include "srb_realization.h"
#include "planck.h"

static res_T
L(float u[4], float x[3], struct ssp_rng* rng, struct context* ctx, double* result)
{
  float range[2];
  struct s3d_hit hit;
  double alpha = 1, sigma = 0;
  struct s3d_primitive prev_prim = S3D_PRIMITIVE_NULL;

  /* Here we go for the diffuse random walk */
  range[0] = 0;
  range[1] = FLT_MAX;
  while(1) {
    /* reset range */
    range[0] = 0;
    /* find geometry distance from x in the u direction */
    do {
      S3D(scene_trace_ray(ctx->scene, x, u, range, &hit));
      /* shift range until no self intersection occurs */
      range[0] += 1.e-6f;
      /* Handle self-hit */
    } while(!S3D_HIT_NONE(&hit) && S3D_PRIMITIVE_EQ(&hit.prim, &prev_prim));
    /* the ray hit a different primitive (or no hit) */
    if (S3D_HIT_NONE(&hit) || f3_dot(u, hit.normal) > 0) {
      /* this is unexpected: invalid x0, or invalid geometry... */
      /* ...or numerical problems */
      return RES_UNKNOWN_ERR;
    }
    /* valid hit! */
    /* Sample a length according to k */
    sigma = ssp_ran_exp(rng, ctx->k);

    if(sigma >= hit.distance) {
      double r, eps;
      float xint[3];
      struct s3d_attrib attr;

      /* sigma exceeds the geometry distance: hit geometry */
      prev_prim = hit.prim;

      /* get intersection position */
      S3D(primitive_get_attrib(&hit.prim, S3D_POSITION, hit.uv, &attr));
      f3_set(xint, attr.value);

      /* get the local properties */
      eps = (*ctx->eps_field_fn)(xint);
      ASSERT(0 <= eps);

      /* bernouilli test */
      r = ssp_rng_canonical_float(rng);
      if (r < eps) {
	/* absorption at xint */
	double t = (*ctx->t_bound_field_fn)(xint);
	double Bxint = planck(t, ctx->lambda);
	ASSERT(0 <= t);
	/* final result */
	*result = alpha * Bxint;
	/* propagation is over */
	break;
      }
      else {
	/* reflection at xint */
	float nx[3];

	/* get normal */
	f3_normalize(nx, hit.normal);

	/* propagation will continue from xint in a new direction */
	ssp_ran_hemisphere_cos(rng, nx, u);
        f3_set(x, xint);
      }
    }
    else {
      /* sigma doesn't exceed the geometry distance: still in the volume */
      double r, ka, ks, kn, kext, pa, ps, pn;
      float tmp[3];

      prev_prim = S3D_PRIMITIVE_NULL;

      /* update position */
      f3_add(x, x, f3_mulf(tmp, u, (float)sigma));

      /* get the local properties */
      ka = (*ctx->ka_field_fn)(x);
      ks = (*ctx->ks_field_fn)(x);
      ASSERT(0 <= ka);
      ASSERT(0 <= ks);
      kext = ka + ks;
      kn = ctx->k - kext;

      /* compute probabilities of absorption, scattering, null collision */
      pa = ka / (kext + fabs(ctx->k - kext));
      ps = ks / (kext + fabs(ctx->k - kext));
      pn = 1 - pa - ps;
      ASSERT(0 <= pa && pa <= 1);
      ASSERT(0 <= ps && ps <= 1);
      ASSERT(0 <= pn && pn <= 1);

      /* bernouilli test */
      r = ssp_rng_canonical_float(rng);
      if (r < pa) {
	/* absorbtion */
	double t = (*ctx->t_fluid_field_fn)(x);
	double Bx = planck(t, ctx->lambda);
	ASSERT(0 <= t);
	/* final result */
	*result = alpha * Bx * ka / (ctx->k * pa);
	/* propagation is over */
	break;
      }
      else if (r < pa + ps) {
	/* scattering */
	double g = (*ctx->g_field_fn)(x);
	ASSERT(-1 <= g && g <= 1);
	/* update weight */
	alpha = alpha * ks / (ctx->k * ps);
	/* propagation will continue in a new u direction */
	ssp_ran_sphere_hg(rng, u, g, u);
      }
      else {
	/* null collision */
	/* update weight */
	alpha = alpha * kn / (ctx->k * pn);
	/* propagation will continue in the same direction */
      }
    }
  }
  return RES_OK;
}

res_T
srb_realization(void* out_bilan, struct ssp_rng* rng, void* context)
{
  struct context* ctx = (struct context*)context;
  double L0, B0, ka0, t0;
  float u0[4], x0[3];
  res_T res;

  /* set x0 */
  f3_set(x0, ctx->x0);

  /* planck at x0 (could be computed just once) */
  ka0 = (*ctx->ka_field_fn)(x0);
  t0 = (*ctx->t_fluid_field_fn)(x0);
  B0 = planck(t0, ctx->lambda);

  /* initial direction u0 */
  ssp_ran_sphere_uniform(rng, u0);

  /* compute L at x0 for direction u0 */
  res = L(u0, x0, rng, ctx, &L0);
  if (res != RES_OK) return res;

  /* final MC weight */
  SMC_DOUBLE(out_bilan) = 4 * PI * ka0 * (L0 - B0);
  return RES_OK;
}
