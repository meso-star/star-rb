/* Copyright (C) |Meso|Star> 2015-2016 (contact@meso-star.com)
 *
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use,
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and, more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms. */

#include <math.h>
#include <rsys/math.h>

#include "planck.h"

static FINLINE double sqr(double x) { return x * x; }

double planck(double T, double lambda) {
  double c1, c2, in;
  const double kBz = 1.3806E-23; /* J/K (Boltzmann constant) */
  const double hPl = 6.62606896E-34; /* J.s (Planck constant) */
  const double c0 = 2.99792458E+8; /* m/s */

  c1 = 2 * PI * hPl * sqr(c0); /* W.m² */
  c2 = hPl * c0 / kBz * 1E6; /* µm.K */
  in = lambda * T;
  if (T == 0)
    return 0;
  else
    /* W/m²/sr/µm */
    return  1E24 / PI * c1 / (pow(in, 5) * (exp(c2 / in) - 1)) * pow(T, 5);
}

