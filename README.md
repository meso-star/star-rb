# Star-RadiativeBudget
This program uses Monte-Carlo to statistically computes radiative budget on 
user defined geometry and fields. The submitted geometry must define a set of
polygonal meshes saved with respect to the Alias Wavefront Obj fileformat. The
resulting surfaces must define a set of closed volumes whose normals point
inward the volumes. 

## How to build

Star-RB relies on [CMake](http://www.cmake.org) and the
[RCMake](https://gitlab.com/vaplv/rcmake/#tab-readme) package to build. It also
depends on the
[RSys](https://gitlab.com/vaplv/rsys/),
[Star-3D](https://gitlab.com/meso-star/star-3d/),
[Star-3DAW](https://gitlab.com/meso-star/star-3daw/),
[Star-MC](https://gitlab.com/meso-star/star-mc/) and the
[Star-SP](https://gitlab.com/meso-star/star-sp/) libraries.

First ensure that CMake is installed on your system. Then install the RCMake
package as well as all the aforementioned prerequisites. Then generate the
project from the `cmake/CMakeLists.txt` file by appending to the
`CMAKE_PREFIX_PATH` variable the install directory of its dependencies.

## License

Star-RB is Copyright (C) |Meso|Star> 2016 (<contact@meso-star.com>).
It is a free software released under the [OSI](http://opensource.org)-approved
CeCILL license. You are welcome to redistribute it under certain conditions;
refer to the COPYING files for details.

