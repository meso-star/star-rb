#!/usr/bin/python
import subprocess # Module used to invoke the "srb" command
import sys
import os
import tempfile

# extract Bounding Box from OBJ
def obj2BBox(obj_file):  
  verts = []  
  for line in open(obj_file, "r"):  
    vals = line.split()  
    if (len(vals) > 0) and (vals[0] == "v"):  
      v = map(float, vals[1:4])  
      verts.append(v)
  xs = [v[0] for v in verts]
  ys = [v[1] for v in verts]
  zs = [v[2] for v in verts]
  return ([min(xs), min(ys), min(zs)], [max(xs), max(ys), max(zs)])

def run_and_draw(obj_file):
  nrealisations = 1000 # realizations per point
  nxpoints = 10 # points on the X axis
  base_name = os.path.splitext(obj_file)[0]
  pdf_b_file = "%s_budget.pdf" % base_name
  content = False
  lambda_ = 1
  model_lower, model_upper = obj2BBox(obj_file)
  half_y =  model_lower[1] + (model_upper[1] - model_lower[1]) / 2
  half_z =  model_lower[2] + (model_upper[2] - model_lower[2]) / 2
  dx = (model_upper[1] - model_lower[1]) / (nxpoints + 1)
  
  # result file
  fb = tempfile.NamedTemporaryFile(delete=False)
  budg_file = fb.name

  # do the simulations and fill the result file up
  for ix in range(0,nxpoints):
    x = model_lower[0] + (1 + ix) * dx
  
    # Generate the srb command
    command = "./srb %s %f %f %f %f %d --quiet" \
               % (obj_file, lambda_, x, half_y, half_z, nrealisations)
  
    # Execute the srb command in the current shell
    process = subprocess.Popen(command, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
  
    # Wait for process termination
    ret = process.wait()
  
    # Retrieve output data
    out, err = process.communicate()
  
    if ret == 0: # OK: keep the result
      list = out.split(' ', 1); # { Radiative Budget, Standard Error }
      # Print gnuplot-ready data: x, budget, stderr
      fb.write("%g %s %s\n" % (x, list[0], list[1]))
      content = True
    # Failure: just skip this point
  
  fb.close()
  
  # use gnuplot to draw in a pdf
  if content:
    command = "gnuplot -e \"set terminal pdf;set output '%s';plot '%s' using 1:2:3 with yerrorbar title 'Radiative Budget'\"" % (pdf_b_file, budg_file)
    process = subprocess.Popen(command, shell=True)
    ret = process.wait()
  
    # open the pdf in evince
    command = "evince %s" % pdf_b_file
    process = subprocess.Popen(command, shell=True)
  else:
    print("Nothing to be drawn")

  # remove tmp
  os.remove(budg_file)

if __name__ == "__main__":
  if len(sys.argv) < 2:
    sys.exit("usage: python %s OBJ_FILE" % sys.argv[0])
  if not os.path.isfile(sys.argv[1]):
    sys.exit("Cannot open %s" % sys.argv[1])
  run_and_draw(sys.argv[1])
